/* Задание 3.8 - Программа, которая запрашивает у пользователя строку, состоящую из нескольких слов и целое число n,
а затем выводит n - ое слово строки на экран */
#include <stdio.h>
#include <string.h>

int main()
{
    char buf[256]; 
    char *wordBegin[128] = { 0 }; //массив для сохранения адресов начала слов в строке
    int length[128] = { 0 }; //массив для сохранения количества символов в словах
    int n, countPos = 0, countWords = 0, countAdd = 0;
    /* countPos - счетчик текущий позиции символа строки, он же счетчик позиции начала слова ;
    countWords- счетчик слов;
    countAdd- дополнительный счетчик для поиска окончания слова либо для сдвига символов в строке */

    puts("Enter a string and press <Enter>");
    fflush(stdin);
    fgets(buf, 256, stdin);
    puts("Enter a number of word and press \"Enter\"");
    fflush(stdin);
    scanf ("%d", &n);
    buf[strlen(buf) - 1] = 0;

    while (buf[0] == ' ') // удаление пробелов в начале строки
    {
        for (countPos = 0; (buf[countPos]); countPos++)
            buf[countPos] = buf[countPos + 1];
    }
    countPos = 0;

    while (buf[countPos])
    {
        if (buf[countPos] != ' ') // работа со словами
        {
            countWords++;
            wordBegin[countWords] = buf + countPos; // сохранение адреса начала слова
            for (countAdd = countPos; buf[countAdd] != ' ' && buf[countAdd] != 0; countAdd++); // поиск позиции окончания слова
            length[countWords] = (countAdd - countPos); // сохранение количества символов в словах (разность позиций окончания и начала слова)
            if (buf[countAdd] == 0) //  Если достигнут конец строки - прервать цикл while
                break;
            else    (countPos = countAdd + 1); // если не конец строки - переход к символу после пробела
        }

        if (buf[countPos] == ' ') // в случае наличия второго подряд пробела осуществляется сдвиг символов строки
        {
            for (; (buf[countAdd]); countAdd++)
                buf[countAdd] = buf[countAdd + 1];
            countAdd = countPos - 1;
        }
    }
    if (n > countWords || n < 1 || countWords == 0)
        printf("Mission impossible=)\n");
    else {
        printf("%.*s\n", length[n], wordBegin[n]);
    }
return 0;
}

