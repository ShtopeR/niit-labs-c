/* Задание 3.1
Подсчет работает корректно если слова состоят целиком из латинских букв или целиком из чисел, строка без знаков препинания и прочих доп. символов
*/
#include <stdio.h>
#include <string.h>
#include <ctype.h>


int main()
{
    char buf[256];
    short countPos = 0, countWords = 0, countNomber = 0; // счётчики: текущей позиции символа/ слов из букв/ чисел 

    puts("Enter a string and press \"Enter\"");
    fflush(stdout);
    fgets(buf, 256, stdin);
    buf[strlen(buf) - 1] = 0;
    
    while (buf[0] == ' ') // удаление пробелов в начале строки
    {
        for (countPos = 0; (buf[countPos]); countPos++)
            buf[countPos] = buf[countPos + 1];
    }
    countPos = 0;
    while (buf[countPos])
    {
      
        // Подсчёт слов из букв:
        if ((buf[countPos] == ' ') && (isalpha(buf[countPos - 1])))
            countWords++;
        if ((isalpha(buf[countPos])) && (buf[countPos + 1] == 0))
            countWords++;
        // Подсчёт чисел:
        if ((buf[countPos] == ' ') && (isdigit(buf[countPos - 1])))
            countNomber++;
        if ((isdigit(buf[countPos])) && (buf[countPos + 1] == 0))
            countNomber++;
      countPos++;
    }
    printf("You entered %d word(s) and %d number(s)\n", countWords, countNomber);
    return 0;
}
