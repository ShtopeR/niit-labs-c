// Задание 3.2 - Почсчёт слов в строке с выводом слов построчно с указанием их длины
#include <stdio.h>
#include <string.h>

int main()
{
    char buf[256];
    short countPos = 0, countWords = 0, countAdd = 0; 
    /* countPos - счетчик текущий позиции символа строки;
    countWords- счетчик слов;
    countAdd- дополнительный счетчик для поиска окончания слова либо для сдвига символов в строке */

    puts("Enter a string and press \"Enter\"");
    fflush(stdin);
    fgets(buf, 256, stdin);
    buf[strlen(buf) - 1] = 0;

    while (buf[0] == ' ') // удаление пробелов в начале строки
    {
        for (countPos = 0; (buf[countPos]); countPos++)
            buf[countPos] = buf[countPos + 1];
    }
    countPos = 0;
    
    while (buf[countPos])
    {
        if (buf[countPos] != ' ') // работа со словами
        {
            countWords++;
            for (countAdd = countPos; buf[countAdd] != ' ' && buf[countAdd] != 0; countAdd++) // вывод текущего слова посимвольно, пока не встретиться пробел или конец строки
                putchar(buf[countAdd]);
            printf(" = %d symbols\n", (countAdd - countPos)); // подсчёт длины слова (кол-во выведенных символов)
            countPos = (countAdd + 1); // переход к следующему символу после пробела
        }
        
        if (buf[countPos] == ' ') // в случае наличия второго подряд пробела осуществляется сдвиг символов строки
        {
            for (; (buf[countAdd]); countAdd++)
                buf[countAdd] = buf[countAdd + 1];
            countAdd = countPos - 1;
        }
    }
    printf("You entered %d words\n", countWords);     
return 0;
}

    