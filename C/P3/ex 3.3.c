// Задание 3.3 - Поиск самого длинного слова в введенной строке, вывод данного слова и его длины
#include <stdio.h>
#include <string.h>

int main()
{
    char buf[256], maxw[256] = { 0 };
    short countPos = 0, countAdd = 0, maxsym = 0, a;
    /* countPos - счетчик текущий позиции символа строки;
    maxsym- кол-во символов самого длинного слова;
    countAdd- дополнительный счетчик для поиска позиции окончания слова либо для сдвига символов в строке
    a- счетчик для копирования значений элементов массива */

    puts("Enter a string and press <Enter>");
    fflush(stdout);
    fgets(buf, 256, stdin);
    buf[strlen(buf) - 1] = 0;

    while (buf[0] == ' ') // удаление пробелов в начале строки
    {
        for (countPos = 0; (buf[countPos]); countPos++)
            buf[countPos] = buf[countPos + 1];
    }
    countPos = 0;

    while (buf[countPos])
    {
        if (buf[countPos] != ' ') // работа со словами
        {
            for (countAdd = countPos; buf[countAdd] != ' ' && buf[countAdd] != 0; countAdd++); // поиск позиции окончания слова
                if ((countAdd - countPos) > maxsym){
                    maxsym = (countAdd - countPos);
                    for (a = 0; a != maxsym; a++)
                        maxw[a] = buf[a + countPos]; // если введено самое длинное слово - копирование его элементов в массив maxw
                }               
            countPos = (countAdd + 1); // переход к следующему символу после пробела
        }

        if (buf[countPos] == ' ') // в случае наличия второго подряд пробела осуществляется сдвиг символов строки
        {
            for (; (buf[countAdd]); countAdd++)
                buf[countAdd] = buf[countAdd + 1];
            countAdd = countPos - 1;
        }
    }
    printf("longer word is\n%s = %d symbols\n", maxw, maxsym);
    return 0;
}