// задание 3.5 - подсчет суммы элементов, расположенным между первым отрицательным и последним положительным элементами целочисленного массива
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>

#define STRING 10 // кол-во элементов массива

int main() {


    short countNegNum = 0, countPosNum, i, result = 0; /* countNegNum - счетчик позиции первого отрицательного числа;
                                                       countPosNum -счетчик позиции последнего положительного числа;
                                                       i - доп. счётчик; result - сумма необходимых элементов */
    short buf[STRING];
    srand(time(0));

    for (i = 0; i < STRING; i++) // заполнение массива
        buf[i] = rand() %21 - 10;

    printf("Source string:\n");
    for (i = 0; i < STRING; i++)
        printf("%d;", buf[i]);

    for (countNegNum = 0; buf[countNegNum] >= 0 && countNegNum < STRING; countNegNum++); //Поиск позиции первого отрицательного числа
    for (countPosNum = STRING - 1; buf[countPosNum] <= 0 && countPosNum > countNegNum; countPosNum--); //Поиск позиции последнего положительного числа
    if (countNegNum == STRING || countPosNum == countNegNum)
        printf("Mission impossible\n");
    else {
        for (i = countNegNum + 1; i < countPosNum; i++) // подсчет суммы значений между позициями countNegNum и countPosNum
            result += buf[i];
        printf("\nThe first negative number is %d.\nThe last positive number is %d.\nsum = %d\n", buf[countNegNum], buf[countPosNum], result);
    }
    return 0;
}