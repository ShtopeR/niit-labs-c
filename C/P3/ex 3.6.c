// задание 3.6 - подсчет суммы элементов, расположенным между  минимальным и максимальным элементами целочисленного массива
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define STRING 10 // кол-во элементов массива
#define RANGE 10  // дипазон возможных значений элементов массива (от 0 до RANGE)

int main() {

    short countMinNum = 0, countMaxNum, i, result = 0; /* countMinNum - счетчик позиции минимального числа;
                                                       countMaxNum -счетчик позиции максимального числа;
                                                       i - доп. счётчик; result - сумма необходимых элементов */
    short buf[STRING];
    srand(time(0));

    for (i = 0; i < STRING; i++) // заполнение массива
        buf[i] = rand() % RANGE + 1;

    printf("Source string:\n");
    for (i = 0; i < STRING; i++)
        printf("%d;", buf[i]);

    for (i = 0, countMinNum = 0; i < STRING; i++){ //Поиск позиции минимального числа
        if (buf[i] < buf[countMinNum])
                countMinNum = i;
    }
    for (i = 0, countMaxNum = 0; i < STRING; i++){  //Поиск позиции максимального числа
        if (buf[i] > buf[countMaxNum])
            countMaxNum = i;
    }
    if (countMinNum < countMaxNum){
        for (i = countMinNum + 1; i < countMaxNum; i++) 
            result += buf[i];
        printf("\nThe min. number is %d.\nThe max. number is %d.\nsum = %d\n", buf[countMinNum], buf[countMaxNum], result);
    }
    else {
        for (i = countMaxNum + 1; i < countMinNum; i++)
            result += buf[i];
        printf("\nThe min. number is %d.\nThe max. number is %d.\nsum = %d\n", buf[countMinNum], buf[countMaxNum], result);
    }
    return 0;
}