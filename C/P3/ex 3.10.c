/* Задание 3.10 - Программа, которая запрашивает у пользователя строку, состоящую
из нескольких слов и целое число n, а затем удаляет n - ое слово из строки */
#include <stdio.h>
#include <string.h>

int main()
{
    char buf[256];
    int length =  0 ; //переменная для сохранения длины считываемого слова
    int n, countPos = 0, countWords = 0, countAdd = 0;
    /* countPos - счетчик текущий позиции символа строки, он же счетчик позиции начала слова ;
    countWords- счетчик слов;
    countAdd- дополнительный счетчик для поиска окончания слова либо для сдвига символов в строке
    n - номер удаляемого слова*/

    puts("Enter a string and press <Enter>");
    fflush(stdin);
    fgets(buf, 256, stdin);
    puts("Enter a number of word and press \"Enter\"");
    fflush(stdin);
    scanf("%d", &n);
    buf[strlen(buf) - 1] = 0;

    while (buf[0] == ' ') // удаление пробелов в начале строки
    {
        for (countPos = 0; (buf[countPos]); countPos++)
            buf[countPos] = buf[countPos + 1];
    }
    countPos = 0;

    while (buf[countPos] && (n > countWords))
    {
        if (buf[countPos] != ' ') // работа со словами
        {
            countWords++;
            for (countAdd = countPos; buf[countAdd] != ' ' && buf[countAdd] != 0; countAdd++); // поиск позиции окончания слова
            length = (countAdd - countPos); // сохранение количества символов в слове (разность позиций окончания и начала слова)           
        }
        if (buf[countAdd] == 0 || n == countWords) //  Если достигнут конец строки или считано требуемое слово - прервать цикл while
            break;
        else countPos = (countAdd + 1); // если не конец строки - переход к символу после пробела
        if (buf[countPos] == ' ') // в случае наличия второго подряд пробела осуществляется сдвиг символов строки
        {
            for (; (buf[countAdd]); countAdd++)
                buf[countAdd] = buf[countAdd + 1];
            countAdd = countPos - 1;
        }
        
    }
    if (n > countWords || n < 1 || countWords == 0)
        printf("Mission impossible=)\n");
    else {
        for (; length >= 0; length--){ // сдвиг всех элементов массива начиная с позиции начала слова, повторяющийся столько раз, сколько символов в удаляемом слове
            countAdd = countPos;
                while (buf[countAdd]){
                    buf[countAdd] = buf[countAdd + 1];
                    countAdd++;
                }
        }
    }
    printf("%s\n", buf);
    return 0;
}

