// Задание 3.4 - Суммирование чисел введённой строки указанной разрядности
#include <stdio.h>
#include <string.h>

#define MAXN 4

int main()
{
    char buf[256], number[MAXN + 1] = { 0 };
    short countPos = 0, countAdd = 0, result = 0, a;
    /* countPos - счетчик текущий позиции символа строки
    countAdd- дополнительный счетчик для поиска позиции окончания числа
    a- счетчик для копирования значений элементов массива
    result- сумма чисел строки */

    puts("Enter a string and press \"Enter\"");
    fflush(stdout);
    fgets(buf, 256, stdin);
    buf[strlen(buf) - 1] = 0;

    while (buf[countPos])
    {
        if (isdigit(buf[countPos])) // работа с цифрами
        {
            for (countAdd = countPos; isdigit(buf[countAdd]) && (countAdd - countPos) < MAXN && buf[countAdd]; countAdd++); // поиск позиции, следующей после окончания числа (не больше допустимой разрядности)
            for (a = 0; a < (countAdd - countPos); a++) // копирование цифр числа в массив number
                number[a] = buf[a + countPos]; 
            if (a < MAXN) //"обрезание" оставшихся элементов массива считываемого числа
                    number[a] = 0;
            result += atoi(number);            
            countPos = countAdd; // переход к позиции символа, следующего за считанным числом
        }
        else countPos++;
    }

    printf("Digital capacity = %d\nresult = %d\n", MAXN, result);
    return 0;
}