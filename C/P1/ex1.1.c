#include <stdio.h>

#define MIN_HEIGHT 50
#define MAX_HEIGHT 250
#define MIN_WEIGHT 40
#define MAX_WEIGHT 200


int main()
{
    int height, weight;
    char gender;

    do {
        printf("Enter your gender (m/w) and press \"Enter\"\n");
        fflush(stdin);
        scanf("%c", &gender);} 
    while (!(gender == 'm' || gender == 'w')); // защита от дурака, не знаю как написать проще
    do {
        printf("Enter your height (cm) and press Enter\n");
        fflush(stdin);
        scanf("%d", &height);}
        while (!(height > MIN_HEIGHT && height < MAX_HEIGHT));
    do {
        printf("Enter your weight (kg) and press Enter\n");
        fflush(stdin);
        scanf("%d", &weight);}
    while (!(weight > MIN_WEIGHT && weight < MAX_WEIGHT));
/* используемая формула: нормальный вес = рост - 100
M и W в ответах используются для проверки корректной работы программы */
switch (gender){
    case 'm':
        if ((height - weight) > 100)
            printf("(M) You should gain %d kg.!\n", (height - weight - 100));
        else if ((height - weight) == 100)
            printf("(M) Your weight is good!\n");
        else
            printf("(M) You should lose %d kg.!\n", (100 - (height - weight)));
        break;
    case 'w':
        if ((height - weight) > 100)
            printf("(W) You should gain %d kg.!\n", (height - weight - 100));
        else if ((height - weight) == 100)
            printf("(W) Your weight is good!\n");
        else
            printf("(W) You should lose %d kg.!\n", (100 - (height - weight)));
        break;
}
return 0;
}
