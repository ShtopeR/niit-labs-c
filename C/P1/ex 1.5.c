#include <stdio.h>
#include <string.h>

#define MAX_SCREEN_WIDTH 80

char str[MAX_SCREEN_WIDTH];
int width;
main()
{
    
    printf("Enter a string (max. %d symbols)\n", MAX_SCREEN_WIDTH);
    fflush(stdin);
    fgets(str, MAX_SCREEN_WIDTH, stdin);
    width = ((MAX_SCREEN_WIDTH + strlen(str)) / 2); // вычисление ширины поля, обеспечивающего размещение строки по центру
    printf("%*s\n", width, str);
    return 0;
}