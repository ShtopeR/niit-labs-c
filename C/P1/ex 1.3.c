#include <stdio.h>


int main()
{
    int control;
    char type;
    float value, result;
    const float p = 3.1416;

    do {
        printf("Enter the value of the angle (xx.xxR / xx.xxD)\n");
        fflush(stdin);
        control = scanf("%f%c", &value, &type);
    } 
    while (control != 2 || value < 0.0 || value > 360.0 || !(type == 'D' || type == 'R'));
   
    if (type == 'D'){
        result = p / 180 * value;
        printf("result %.2fR\n", result);
    }
    else {
        result = 180 / p * value;
        printf("result %.2fD\n", result);
    }
return 0;
}