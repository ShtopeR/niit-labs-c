#include <stdio.h>


int main()
{
    int foot, inch, control;
    float height;
    const float d = 2.54; // one inch is equal to 2.54 centimetres

    do {
        printf("Enter your height in format xx,yy (xx - a quantity of foots, yy - a quantity of inches) and press \"Enter\"\n");
        fflush(stdin);
        control = scanf("%d,%d", &foot, &inch);
    } while (control != 2 || !(foot >= 0 && foot < 9) || !(inch >= 0 && inch < 12)); // проверка корректности введенных данных
    
        height = (d * inch) + (12 * d * foot); // The foot is equal 12 inches
        printf("your height is equal to %.2f cm.\n", height);
        
    return 0;
}