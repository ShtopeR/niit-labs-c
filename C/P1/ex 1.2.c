#include <stdio.h>
int main()
{
    unsigned int hh, mm, ss, result;

    do {
        printf("Enter the current time (HH:MM:SS) and press \"Enter\"\n");
        fflush(stdin);
        result = scanf("%d:%d:%d", &hh, &mm, &ss);
    } 
    while ((result != 3) || (hh > 23) || (mm > 59) || (ss > 59)); // Защита от дурака
   
    if (hh > 5 && hh < 12)
        printf("Good morning!\n");
    else if (hh > 11 && hh < 17)
        printf("Good afternoon!\n");
    else if (hh > 16 && hh < 22)
        printf("Good evening!\n");
    else
        printf("Good night!\n");
return 0;
}