#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#define MAXPASS 10 // кол-во создаваемых паролей
#define MAXSYM 8 // кол-во символов в пароле

int main()
{
    char arr[9];
    short countPas, countSym , var, control_a, control_B, control_0; // countPas -счетчик паролей, countSym - счетчик символа в пароле, var - варинты диапазонов значений, control_? - анализ наличия всех типов символов

    srand(time(0));
    for (countPas = 0 ; countPas < MAXPASS; countPas++)
    {
        control_a = 0; control_B = 0; control_0 = 0;
        for (countSym = 0; countSym < MAXSYM; countSym++)
        {
            var = rand() % 3;
            switch (var) // присваивание текущему символу случайного значения случайного диапазона
            {
            case 0: arr[countSym] = (rand() % ('z' - 'a' + 1) + 'a');
                control_a++;
                break;
            case 1: arr[countSym] = rand() % ('Z' - 'A' + 1) + 'A';
                control_B++;
                break;
            case 2: arr[countSym] = rand() % ('9' - '0' + 1) + '0';
                control_0++;
                break;
            }
            
        }
    if (control_a == 0 || control_B == 0 || control_0 == 0)
        countPas--; // повтор в случае отсутствия символов одного из типов
    arr[MAXSYM] = 0;
    printf("%s\n", arr);
    }
    return 0;
}