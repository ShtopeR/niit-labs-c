// удаление лишних пробелов в строке

#include <stdio.h>
#include <string.h>

int main()
{
    char buf[256];
    int i = 0, z; // i - счетчик символа в строке, z - вспомогательный счетчик для сдвига строки

    puts("Enter a string:");
    fgets(buf, 256, stdin);
    buf[strlen(buf) - 1] = 0;

 while (buf[i])
 {
    z = i; // для сдвига строки (при необходимости) с текущего символа
    if (buf[0] == ' ')
        {
        for (z = 0; (buf[z]); z++)
            buf[z] = buf[z + 1];
        }
    if (buf[i] == ' ' && buf[i + 1] == ' ')
        {
        for (z = i; (buf[z]); z++)
            buf[z] = buf[z + 1];
            i--; // для удаления более 2ух пробелов подряд
        }
    if (buf[i] == ' ' && buf[i + 1] == 0)
        buf[i] = 0;
        
    i++;
 }
 printf("%s\nString = %d symbols\n", buf, strlen(buf));
return 0;
}

    
    