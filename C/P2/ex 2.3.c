#include <stdio.h>

int main()
{
    int countString, countSymbol, countSpace, up; // countString - счётчик строк, countSpace - счётчик пробелов, countSymbol - счётчик *, up - высота пирамиды
    puts("Enter a height of pyramid");
    scanf("%d", &up);
    for (countString = 1; countString <= up ; ++countString) // доводим кол-во строк до необходимой высоты пирамиды
    {
        for (countSpace = 0; countSpace < (up - countString); ++countSpace) // пока пробелов не станет равным (высота пирамиды - номер строки)
            putchar(' ');
        for (countSymbol = 0; countSymbol<(2 * countString - 1); ++countSymbol) // пока кол-во * не станет (2*номер строки -1)
            putchar ('*');

        putchar('\n');
    }
    return 0;
}