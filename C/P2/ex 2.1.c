#include <stdio.h>
#include <time.h>

int main()
{

clock_t now;
int  t = 0, control = 0; //  t - timer
float height = 0., way = 0.; // way - a way bomb
const float g = 9.81; // gravitational acceleration

do {
    printf("Enter a height (m.) and press \"Enter\".\n");
    fflush(stdin);
    control = scanf("%f", &height);
    }
while ((control != 1 || height <= 0)); // foolproof

while (height > way)
	{
        printf("t = %d s., h = %.2f\n", t, (height - way));
		t++;		
        way = ((g * (t * t)) / 2);
		now = clock();
		while (clock() < now + CLOCKS_PER_SEC);
	}
	printf("BABAH!\n");

return 0;
}