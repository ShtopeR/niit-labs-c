// подсчёт встречаемости символов в строке

#include <stdio.h>
#include <string.h>

int main()
{
	char buf[256];
	int counts[256] = { 0 };
	short i = 0;

	puts("Enter a string:");
	fflush(stdin);
	fgets(buf, 256, stdin);
	buf[strlen(buf) - 1] = 0;

	while (buf[i])
		counts[buf[i++]]++;
	i = 0;
	while (i < 256)
	{
		if (counts[i])
			printf("%c - %d\n",i ,counts[i]);
		i++;
	}
		
	return 0;
} 