#include <stdio.h>
#include <stdlib.h>

#define MAX 100 // верхняя граница диапазона загадываемого числа

int main()
{
    unsigned int number, x; // number - вводимое пользователем число, x - загаданное число
    
    srand(time(0));
    x = rand()%(MAX +1);
   
    printf("I think of some number from 0 to %d. Can you guess it?\n", MAX);
    fflush(stdin);
    scanf("%d", &number);
    
    while (x != number)
    {
        if (number > x) {
            printf("You don`t guess, my number is smaller. Try one more time.\n");
            fflush(stdin);
            scanf("%d", &number);
            }
        else{
            printf("You don`t guess, my number is bigger. Try one more time.\n");
            fflush(stdin);
            scanf("%d", &number);
            }
    }
    printf("You have guessed right! This is number %d!\n", x);
        
return 0;
}