/*Задание 5.2 - Калейдоскоп*/

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <windows.h>

#define WIDTH 160 // длина поля
#define HEIGHT 50 // ширина поля
#define XX 100 // количество генерируемых звёздочек на 1/4 экрана (поля)

char canvas[HEIGHT][WIDTH];

void clearCanvas()
{
    int i, j;
    for (i = 0; i < HEIGHT; i++)
        for (j = 0; j < WIDTH; j++){
            canvas[i][j] = ' ';
        }
}

void fillQuarterCanvas()
{
    int i, j;
    int count = XX;
    while (count > 0)
    {
        i = rand() % (HEIGHT / 2);
        j = rand() % (WIDTH / 2);
        if (canvas[i][j] == ' '){
            canvas[i][j] = '*';
            count--;
        }
    }
}
void copyCanvas()
{
    int i, j;
    for (i = 0; i < HEIGHT; i++)
        for (j = 0; j < WIDTH; j++)
        {
            if (canvas[i][j] == '*')
            {
                canvas[HEIGHT - i][j] = '*';
                canvas[HEIGHT - i][WIDTH - j] = '*';
                canvas[i][WIDTH - j] = '*';
            }
        }
}


void paintCanvas()
{
    int i, j;
    for (i = 0; i < HEIGHT; i++)
        for (j = 0; j < WIDTH; j++)
            putchar(canvas[i][j]);
    putchar('\n');
}


int main()
{    
    while (1)
    {
        system("cls");
        srand(time(NULL));
        clearCanvas();
        fillQuarterCanvas();
        copyCanvas();
        paintCanvas();
        Sleep(3000);
    }
    return 0;
}