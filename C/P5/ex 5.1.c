/* Задание 5.1 - Программа,  которая принимает от пользователя строку и
выводит ее на экран, перемешав слова в случайном порядке.*/

#include <stdio.h>
#include <string.h>
#include <time.h>

#define MAXWORDS 128
#define MAXSYMBOLS 256

int printWord (char *); // вывод слова посимвольно, пока не достигнет пробела или конца строки, возвращает кол-во выведенных символов
int getWords (char *); // заполняет массив указателей позициями начала слов, возвращает кол-во слов в строке


char *wordBegin[MAXWORDS] = { 0 }; //массив для сохранения адресов начала слов в строке

printWord(char *wbegin)
{
    int i;
    for (i = 0; (*(wbegin) && *(wbegin) != ' '); wbegin++, i++)
        printf("%.1s", wbegin);
    printf(" ");
    return i;
}
int getWords(char *stringBegin)
{
    int inword = 0;
    int count = 0;
    while (*stringBegin)
    {
        if (*stringBegin != ' ' && inword == 0)
        {
            count++;
            wordBegin[count] = stringBegin;
            inword = 1;
        }
        if (*stringBegin == ' ')
            inword = 0;
        stringBegin++;
    }
    return count;
}

int main()
{
    char buf[MAXSYMBOLS];
    char *temp; // указатель для временного хранения адреса перемещаемой ячейки массива
    int countWords = 0, randw = 0;
    /* countWords- счетчик слов; randw - порядковый номер случайного слова */

    srand(time(0));

    puts("Enter a string and press \"Enter\"");
    fflush(stdin);
    fgets(buf, 256, stdin);
    buf[strlen(buf) - 1] = 0;

    countWords = getWords(buf);

    while (countWords > 0)
    {
        randw = rand() % countWords + 1;// генерирование случайного номера слова из оставшегося кол-ва слов
        temp = wordBegin[randw]; // перемещение указателя на случайное слово в ячейку, соответствующую номеру слова
        while (wordBegin[randw]){ 
            wordBegin[randw] = wordBegin[randw + 1];
            randw++;
        }
        wordBegin[countWords] = temp;
        printWord(wordBegin[countWords]); // вывод слова, указанного в ячейке массива, в зависимости от номера слова
        countWords--;
    }    
    printf("\n");    
    return 0;
}