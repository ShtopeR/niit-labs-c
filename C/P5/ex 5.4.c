/* Задание 5.4 - программа, которая читает построчно текстовый файл и
переставляет случайно слова в каждой строке */
#include <stdio.h>
#include <string.h>
#include <time.h>

#define MAXWORDS 128
#define MAXSTRING 256

//int printWord(char *); // вывод слова посимвольно, пока не достигнет пробела или конца строки, возвращает кол-во выведенных символов
int getWords(char *); // заполняет массив указателей позициями начала слов, возвращает кол-во слов в строке

char *wordBegin[MAXWORDS] = { 0 }; //массив для сохранения адресов начала слов в строке

printWordToFile(char **wbegin, FILE **fp)
{
    int i;
    for (i = 0; *(*wbegin + i) && *(*wbegin + i) != ' '; i++)
        fputc(*(*wbegin + i), *fp);
    fputc(' ', *fp);
}

int getWords(char *stringBegin)
{
    int inword = 0;
    int count = 0;
    while (*stringBegin)
    {
        if (*stringBegin != ' ' && inword == 0)
        {
            count++;
            wordBegin[count] = stringBegin;
            inword = 1;
        }
        if (*stringBegin == ' ')
            inword = 0;
        stringBegin++;
    }
    return count;
}

int main()
{
    char string[MAXSTRING];
    char *temp; // указатель для временного хранения адреса перемещаемой ячейки массива
    int countWords = 0, randW = 0, i;
    /* countWords- счетчик слов; randW - порядковый номер случайного слова */
    FILE *fp1, *fp2;

    srand(time(0));
    fp1 = fopen("1.txt", "rt");
    fp2 = fopen("2.txt", "w");
    if ((fp1 == 0) || (fp2 == 0))
    {
        perror("File error:");
        exit(1);
    }
    fprintf(fp2, "Words before/after sorting:\n");
    while (fgets(string, 256, fp1)){
        string[strlen(string) - 1] = 0;
        fprintf(fp2, "%s\n", string);
        countWords = getWords(string);
        while (countWords > 0)
        {
            randW = rand() % countWords + 1;// генерирование случайного номера слова из оставшегося кол-ва слов
            temp = wordBegin[randW]; // сохранение указателя на случайное слово
            while (wordBegin[randW]){ // сдвиг массива
                wordBegin[randW] = wordBegin[randW + 1];
                randW++;
            }
            wordBegin[countWords] = temp; // обмен указателя последнего слова в текущей итерации на указатель на случайное слово
            printWordToFile(&wordBegin[countWords], &fp2); // вывод слова в файл
            countWords--;
        }
        fputc('\n', fp2);
    }
    fcloseall();
    puts("strings were reading from file \"1.txt\", words were sorting and writing to file \"2.txt\".\n");
    return 0;
}