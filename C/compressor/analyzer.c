#include <stdio.h>
#include <stdlib.h>
#include "compressor.h"

extern TSYM * pSyms[256];

void analyzer(FILE *fp, TSYM* syms, THEAD *header)
{
    float summ = 0;
    int ch, counts[256] = { 0 };
    unsigned int i = 0, j = 0, countUniqueSym = 0, countSym = 0;    // countSym - счетчик общего кол-ва символов (байтов) в сжимаемом файле
         
    while ((ch = fgetc(fp)) != EOF)
    {
        counts[ch]++;
        countSym++;
    }
    header->sizeFile = countSym;
    while (i < 256)
    {
        if (counts[i])
        {
            countUniqueSym++; // подсчет уникальных символов (байтов) в файле
            (syms + j)->ch = i;
            (syms + j)->freq = (float) counts[i] / countSym;            
            j++;
        }
    i++;
    }
    header->countUniqueSym = countUniqueSym;
    //qsort(syms, countUniqueSym, sizeof(TSYM), compare); // неудавшийся вариант
    sortSyms(syms, countUniqueSym);
    for (i = 0; i < countUniqueSym; i++) // заполнение массива указателей на структуры
        pSyms[i] = &syms[i];
    
}

static void sortSyms(TSYM* syms, int countUniqueSym) // сортировка массива структур по убыванию частоты
{
    int i = 0, j, numMax = -1;
    float max;
    TSYM tempSym = { 0 };
    while (i < countUniqueSym)
    {
        for (j = i, max = 0; j < countUniqueSym; j++)
        {
            if ((syms + j)->freq > max)
            {
                max = (syms + j)->freq;
                numMax = j;
            }
        }
        if (i == numMax);
        else
        {
            tempSym = *(syms + i);
            *(syms + i) = *(syms + numMax);
            *(syms + numMax) = tempSym;
        }
        i++;
    }
}

void getName(char *fileName, THEAD *header)
{
    int i = 0, j = 0;
    char name[256] = { 0 }, extension[3] = { 0 };
    strcpy(name, fileName);
    //while (name[i] != "." && name[i])
    while (*(fileName + i) != '.')  // копирование имени исходного файлав структуру загаловка
    {
        header->fileName[i] = *(fileName + i);
        i++;
    }
    i++; // пропуск точки в имени файла
    while (*(fileName + i)) // копирование расширения в структуру загаловка
    {
        header->native[j] = *(fileName + i);
        i++;
        j++;
    }
}