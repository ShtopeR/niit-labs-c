#include <stdio.h>
#include <stdlib.h>
#include "compressor.h"
#define BYTE 8


void packer(FILE *fp_temp, FILE *fp_result, THEAD *header)
{
    unsigned char buf[BYTE], ch, packChar;
    int i = 0, j;
    
    printHeader(fp_result, header);
    while (i < header->sizeFile)
    {
        for (j = 0; j < 8; j++)
        {
            ch = fgetc(fp_temp);
            buf[j] = ch;
        }
        packChar = pack(buf);
        fputc(packChar, fp_result);
        i += BYTE;
    }
}

unsigned char pack(unsigned char buf[])
{
    union CODE code;
    code.byte.b1 = buf[0] - '0';
    code.byte.b2 = buf[1] - '0';
    code.byte.b3 = buf[2] - '0';
    code.byte.b4 = buf[3] - '0';
    code.byte.b5 = buf[4] - '0';
    code.byte.b6 = buf[5] - '0';
    code.byte.b7 = buf[6] - '0';
    code.byte.b8 = buf[7] - '0';
    return code.ch;
}

void printHeader(FILE* fp_result, THEAD *header)
{

}