#include <stdio.h>
#include <stdlib.h>
#include "compressor.h"

extern TSYM syms[256];
extern TSYM * pSyms[256];

struct SYM* buildTree(struct SYM **psym, unsigned short N)
{
    int i, j;
    // создаём временный узел
    struct SYM *temp = (struct SYM*)malloc(sizeof(struct SYM));
    // в поле частоты записывается сумма частот
    // последнего и предпоследнего элементов массива psym
    temp->freq = psym[N - 2]->freq + psym[N - 1]->freq;
    // связываем созданный узел с двумя последними узлами
    temp->left = psym[N - 1];
    temp->right = psym[N - 2];
    temp->code[0] = 0;
    if (N == 2) // мы сформировали корневой элемент с частотой 1.0
        return temp;
    // добавляем temp в нужную позицию psym,
    // сохраняя порядок убывания частоты
    else
    {
        for (i = 0; i < N; i++)
        {
            if (temp->freq > psym[i]->freq)
            {
                for (j = N - 1; j > i; j--)
                    psym[j] = psym[j - 1];
                psym[i] = temp;
                break;
            }
        }
        return buildTree(psym, N - 1);        
    }
}

void makeCodes(struct SYM *root)
{
    if (root->left)
    {
        strcpy(root->left->code, root->code);
        strcat(root->left->code, "0");
        makeCodes(root->left);
    }
    if (root->right)
    {
        strcpy(root->right->code, root->code);
        strcat(root->right->code, "1");
        makeCodes(root->right);
    }
}