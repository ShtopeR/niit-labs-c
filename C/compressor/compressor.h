#include <stdio.h>

struct SYM
{
    unsigned char ch; // ASCII-код
    float freq; // частота встречаемости
    char code[256]; // массив для нового кода
    struct SYM *left; // левый потомок в дереве
    struct SYM *right; // правый потомок в дереве
};
typedef struct SYM TSYM;

union CODE {
    unsigned char ch;
    struct {
        unsigned short b1 : 1;
        unsigned short b2 : 1;
        unsigned short b3 : 1;
        unsigned short b4 : 1;
        unsigned short b5 : 1;
        unsigned short b6 : 1;
        unsigned short b7 : 1;
        unsigned short b8 : 1;
    } byte;
};
typedef union CODE CODE;

struct TABLE_FREQ
{
    unsigned char sym;
    float freq;
};
typedef struct TABLE_FREQ table_freq;

struct HEAD
{
    char *signature;
    char native [3]; // расширение исходного файла
    unsigned short countUniqueSym;    // кол-во уникальных символов (байтов) в файле
    char fileName [256]; // имя исходного файла
    int tail;   // длина хвоста
    int sizeFile;    // размер исходного файла
    table_freq syms [256];
};
typedef struct HEAD THEAD;

void getName(char *fileName, THEAD *header);
void analyzer(FILE *fp, TSYM* syms, THEAD *header);
void sortSyms(TSYM* syms, int countUniqueSym);
struct SYM* buildTree(struct SYM **psym, unsigned short N);
void makeCodes(struct SYM *root);
int getTail(FILE *fp_101);
void coder(FILE *fp_in, FILE *fp_101, THEAD *header);
unsigned char pack(unsigned char buf[]);
void printHeader(FILE* fp_result, THEAD *header);
void packer(FILE *fp_temp, FILE *fp_result, THEAD *header);
void makeHead(FILE *fp_result);