#include <stdio.h>
#include <string.h>
#include "compressor.h"
 

TSYM syms[256];
TSYM * pSyms[256];


int main(int argc, char** argv)
{
    unsigned int countSym;
    char name[256] = { 0 };
    char fileExt[3] = { 0 };
    FILE *fp_in, *fp_temp, *fp_result;
    TSYM *root;
    THEAD header = { 0 };

    if (argc < 3) {
        puts("Usage FAQ");
        exit(1);
    }
    fp_in = fopen(argv[1], "rb");
    fp_result = fopen(argv[2], "wb");
    fp_temp = fopen("temp.101", "wb");
    if (fp_in == NULL || fp_result == NULL || fp_temp == NULL)
    {
        perror("File error:");
        exit(2);
    }
    strcpy(name, argv[1]);
    getName(name, &header);
    analyzer(fp_in, syms, &header);
    root = buildTree(&pSyms, header.countUniqueSym);
    makeCodes(root);
    rewind(fp_in);
    coder(fp_in, fp_temp, &header);
    fclose(fp_temp);
    fp_temp = fopen("temp.101", "rb");
    packer(fp_temp, fp_result, &header);
    fcloseall();
    printf("The file %s was compressed to file %s\n", argv[1], argv[2]);
    return 0;
}