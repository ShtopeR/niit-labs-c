#include <stdio.h>
#include "compressor.h"
#define BYTE 8

extern TSYM syms[256];

void coder(FILE *fp_in, FILE *fp_101, THEAD *header) // fp_in - исходный файл; fp_101 - закодированный файл
{
    int ch, i; // код символа из файла
    while ((ch = fgetc(fp_in)) != EOF)
        {
            for (i = 0; i < header->countUniqueSym; i++)
                if (syms[i].ch == (unsigned char)ch) {
                    fputs(syms[i].code, fp_101); // выводим строку с кодом
                    break; // прерываем поиск
                }
        }
    fclose(fp_101);
    fp_101 = fopen("temp.101", "rb");
    header->tail = getTail(fp_101);
    fclose(fp_101);
}

int getTail(FILE *fp_101)
{
    int ch, tail;
    unsigned long size = 0;
    
    while ((ch = fgetc(fp_101)) != EOF)
        size++;
    if (size %BYTE)
        tail = BYTE - size %BYTE;
    else tail = 0;
    return tail;
}