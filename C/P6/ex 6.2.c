/* Задание 6.2 - программа, которая находит в диапазоне целых чисел от
2 до 1000000 число, формирующее самую длинную последовательность Коллатца*/

#include <stdio.h>

#define MIN_N 2
#define MAX_N 1000000

unsigned int collatz(unsigned int n)
{
    unsigned int step;
    unsigned static int count = 0;
    if (n == 1){
        step = count;
        count = 0;
        return step;
    }    
    else if (n % 2)
        n = n * 3 + 1;
    else
        n /= 2;
    count++;
    collatz (n);
}

int main()
{
    unsigned int n, step, maxStep = 0, num;

    for (n = MIN_N; n <= MAX_N; n++)
    {
        step = collatz(n);
        //printf("%d - %d\n", n, step);
        if (step > maxStep)
        {
            maxStep = step;
            num = n;
        }
    }
    printf("Max. steps takes %d for number %d\n", maxStep, num);
    return 0;
}