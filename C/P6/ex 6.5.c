/* Задание 6.5 - Программа, которая измеряет время вычисления N-ого
члена ряда Фибоначчи */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MIN_N 1
#define MAX_N 40

unsigned long fib (int n)
{
    if (n == 1 || n == 2)
        return 1;
    else return fib(n - 1) + fib(n - 2) ;
}

int main()
{
    clock_t begin, end;
    int time;
    int n = 0;
    FILE *fp;
    fp = fopen("result.txt", "wt");
    if (fp == NULL){
        puts("File error");
        perror(1);
    }
    for (n = MIN_N; n <= MAX_N; ++n)
    {
        begin = clock ();
        printf("N=%d, %ld ", n, fib(n));
        fprintf(fp, "N=%d, %ld ", n, fib(n));
        end = clock ();
        time = (end - begin) / CLOCKS_PER_SEC;
        printf("time = %d\n", time);
        fprintf(fp, "time = %d\n", time);
    }
    fclose(fp);
    return 0;
}