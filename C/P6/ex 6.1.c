// Задание 6.1 - Фрактал
#include <stdio.h>

#define SIZE 30

char canvas[SIZE][SIZE];

void clearCanvas()
{
    int i, j;
    for (i = 0; i < SIZE; i++)
        for (j = 0; j < SIZE; j++){
            canvas[i][j] = ' ';            
        }
}
int power(int k)
{
    int i;
    int val = 1;
    for (i = 0; i < k; i++)
        val *= 3;
    return val;
}

void drawFractal(int x, int y, int k) // y - координата центра k - коэффициент
{
    if (k == 0)
        canvas[y][x] = '*';
    else
    {
        
        drawFractal(x, y - power(k-1), k - 1);
        drawFractal(x + power(k-1), y, k - 1);
        drawFractal(x, y, k - 1);
        drawFractal(x, y + power(k-1), k - 1);
        drawFractal(x - power(k-1),y , k - 1);
    };
}

void showFractal()
{
    int i,j;
    for (i = 0; i < SIZE; i++){
        for (j = 0; j < SIZE; j++)
            putchar(canvas[i][j]);
        putchar('\n');
    }
}

void main(){
    clearCanvas();
    drawFractal(SIZE / 2, SIZE / 2, 3);
    showFractal();
    return 0;
}