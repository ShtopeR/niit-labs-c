/* Задание 6.7 - выход из лабиринта*/
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <windows.h>

#define WIDTH 29  // длина лабиринта
#define HEIGHT 9 // ширина лабиринта

char labirint[HEIGHT][WIDTH];

void readLabirintFromFile(FILE **fp) // чтение лабиринта из файла
{
    int j = 0;
    while (fgets(labirint[j], 256, *fp))
        j++;
}

void paintLabirint() // вывод массива "лабиринта" на экран
{
    Sleep(200);
    system("cls");
    int i, j;
    for (j = 0; j < HEIGHT; j++){
        for (i = 0; i < WIDTH; i++)
            putchar(labirint[j][i]);
        putchar('\n');
    }
}

int moveX(int j, int i) // движение "человека"
{    
    char x = 'X';
    int dir;
    labirint[j][i] = x;
    paintLabirint();

        if (i == 0 || i == WIDTH || j == 0 || j == HEIGHT) // если достигнута граница массива - найден выход
        {
            printf("\nThe win!!!\n");
            getchar();
            exit(0);
        }

        if (labirint[j - 1][i] != ' ' && labirint[j + 1][i] != ' ' && labirint[j][i - 1] != ' ' && labirint[j][i+1] != ' ') // если нет "ходов" - возврат
        {
            labirint[j][i] = '*';
            return;
        }
        if (j > 0 && labirint[j - 1][i] == ' ') // движение вверх
        {

            labirint[j][i] = '*';
            moveX(j - 1, i);
        }
        if (i < WIDTH && labirint[j][i + 1] == ' ') // движение вправо
            {
                labirint[j][i] = '*';
                moveX(j, i + 1);
            }
        if (j < HEIGHT && labirint[j + 1][i] == ' ') // движение вниз
            {
                labirint[j][i] = '*';

                moveX(j + 1, i);
            }

       if (i > 0 && labirint[j][i - 1] == ' ') // движение влево
            {
                labirint[j][i] = '*';

                moveX(j, i - 1);
            }
    }

int main()
{
    FILE *fp;
    fp = fopen("Lab.txt", "rt");
    if (fp == NULL){
        puts("File error");
        exit(1);
    }
    readLabirintFromFile(&fp);
    moveX(HEIGHT / 2, WIDTH / 2 );
    return 0;
}