/* Задание 4.4 - Программа, определяющуя в строке символьную последовательность
максимальной длины (с использованием указателей) */
#include <stdio.h>
#include <string.h>

int main()
{
    char buf[256];
    char *MaxSucBegin = 0, *SucBegin = 0; //указатели для сохранения адресов начала считываемой и максимальной последовательностей
    int maxSuc = 0, lengthSuc, countPos = 0;
    /* lengthSuc - длина считываемой последовательности
    maxSuc - длина максимальной последовательности
    countPos - счетчик текущий позиции символа строки */

    puts("Enter a string and press \"Enter\"");
    fflush(stdin);
    fgets(buf, 256, stdin);

    buf[strlen(buf) - 1] = 0;

    while (buf[countPos])
    {
        if (buf[countPos] == buf[countPos + 1]) // работа c последовательностями
        {
            SucBegin = buf + countPos;
            for (lengthSuc = 1; buf[countPos] == buf[countPos + 1]; countPos++, lengthSuc++); // определение длины последовательности
            if (lengthSuc > maxSuc) // сохранение значения и адреса максимума
            {
                maxSuc = lengthSuc;
                MaxSucBegin = SucBegin;
            }
        }
        else countPos++;
    }

    if (maxSuc > 0)
        printf("max succession is \"%.*s\"\n", maxSuc, MaxSucBegin);
    else printf("succession was not found\n");

    return 0;
}