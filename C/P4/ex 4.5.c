/* программа, сортирующая строки (см. задачу 4.1), но использующая
строки, прочитанные из текстового файла. Результат работы программы также
записывается в файл.*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAXSTRING 30 // макс. кол-во принимаемых строк

char strings[MAXSTRING][256];
char *strBeg[MAXSTRING] = { 0 };


void SwapP(char **strBeg, char **strBegMax) // функция обмена значений двух элементов массива указателей
{
    char* T;
    T = *strBeg;
    *strBeg = *strBegMax;
    *strBegMax = T;
}

void SortArrMax(char ** Arr, int count) // функция сортировки элементов массива указателей
{
    int i, j, max = 0, numMaxArr;
    for (j = 0; j < count; j++){
        for (i = j, max = 0; i < count; i++)
        {
            if (max < strlen(*(Arr + i))){
                max = strlen(*(Arr + i));
                numMaxArr = i;
            }
        }
        if (j == numMaxArr)
            continue;
        else
            SwapP((strBeg + j), (strBeg + numMaxArr));
    }
}

int main()
{
    int n, countStr = 0; // countStr - счетчик принятых строк
    
    FILE *fp;
    
    fp = fopen("1.txt", "a+t");
    if (fp == 0)
    {
        perror("File error:");
        exit(1);
    }    
    while (fgets(strings[countStr], 256, fp) && (countStr < MAXSTRING))
    {
        strBeg[countStr] = strings[countStr]; // заполнение массива указателей адресами строк
        countStr++;
    }
   
    SortArrMax(strBeg, countStr);    
    fprintf(fp, "\nStrings after sorting:\n");
    for (n = 0; n < countStr; n++)
        fprintf(fp, "%s", *(strBeg + n));
    fclose(fp);
    puts("strings were reading from file and sorting\n");
    return 0;
}