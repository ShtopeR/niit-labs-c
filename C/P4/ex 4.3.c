/* Задание 4.3 - Программа, которая запрашивает строку и определяет, не является
ли строка палиндромом (одинаково читается и слева направо и справа налево) */

#include <stdio.h>

#define MAXSTRING 256

int main()
{
    char buf[MAXSTRING];
    char *begin;
    char *end;
    int flag = 0, b, e; // b, e - счетчики положения считываемых символов с начала и с конца строки

    puts("Input a string and press \"Enter\"\n");
    fgets(buf, MAXSTRING, stdin);
    buf[strlen(buf) - 1] = 0;

    begin = &buf;
    end = &buf[strlen(buf)] - 1;

    for (b = 0, e = strlen(buf); b < e; b++, e--, begin++, end--)
    {
        if (*begin == ' '){
            begin++;
            b++;
        }
        if (*end == ' '){
            end--;
            e--;
        }
        if (*begin != *end){
            flag = 1;
            break;
        }
    }    
    (flag == 0) ? puts("Your string is palindrom\n") : puts("Your string isn`t palindrom");

    return 0;
}