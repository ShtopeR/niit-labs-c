#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAXSTRING 30 // макс. кол-во принимаемых строк

char strings[MAXSTRING][256];
char *strBeg[MAXSTRING] = { 0 };

void SwapP(char **strBeg, char **strBegMax) // функция обмена значений двух элементов массива указателей (применение - SwapP((strBeg + 0), (strBeg + 1));)
{
    char* T;
    T = *strBeg;
    *strBeg = *strBegMax;
    *strBegMax = T;
}

void SortArrMax(char ** Arr, int count) // функция сортировки элементов массива указателей
{
    int i, j, max = 0, numMaxArr;
    for (j = 0; j < count; j++){
        for (i = j, max = 0; i < count; i++)
        {
            if (max < strlen(*(Arr + i))){
                max = strlen(*(Arr + i));
                numMaxArr = i;
            }
        }
        if (j == numMaxArr)
            continue;
        else
            SwapP((strBeg + j), (strBeg + numMaxArr));
    }
}

int main()
{
    int n, countStr = 0; // countStr - счетчик принятых строк

    do
        {
            puts("Enter a string:");
            fgets(strings[countStr], 256, stdin);
            strings[countStr][strlen(strings[countStr]) - 1] = 0; // удаление \n в конце принятых строк
            strBeg[countStr] = strings[countStr]; // заполнение массива указателей адресами строк
            countStr++;
        } 
    while ((strlen(strings[countStr - 1])>0) && (countStr < MAXSTRING));
    countStr -= 1; // удаление из счетчика последней, пустой строки
    SortArrMax(strBeg, countStr);    
    for (n = 0; n < countStr; n++) 
        printf("%s\n", *(strBeg + n));
return 0;
}