/* Задание 4.6 - программа, которая запрашивает родственников и определяет самого молодого и самого старого */

#include <stdio.h>
// #include <string.h>
#define MAXFAM 10 // максимальное количество членов семьи
#define MAXNAME 100 // максимальная длина имени

int main()
{
    int countMemFam, numMem = 0, age, maxAge = 0, minAge = 1000; // countMemFam - кол-во членов семьи, numMem - счетчик для внесения имен
    char *young, *old;
    char name [MAXFAM][MAXNAME]; // массив строк для хранения имен

    do {
        puts("Input the number of family members and press \"enter\"\n");
        fflush(stdin);
        scanf("%d", &countMemFam);
        if (countMemFam > MAXFAM)
            puts("This is too much family for this programm\n");        
    } while (countMemFam > MAXFAM);

    while (countMemFam > 0)
    {
        printf("Input a name of family member number %d end press \"enter\"\n", numMem + 1);
        fflush(stdin);
        gets(name[numMem]);
        printf("Input an age of this family member end press \"enter\"\n", numMem + 1);
        fflush(stdin);
        scanf("%d", &age);
        if (age > maxAge)
        {
            maxAge = age;
            old = name[numMem];
        }
        if (age < minAge)
        {
            minAge = age;
            young = name[numMem];
        }
        numMem++;        
        countMemFam--;
    }
    printf("The name of the youngest family member is \"%s\" (%d years old)\n", young, minAge);
    printf("The name of the oldest family member is \"%s\" (%d years old)\n", old, maxAge);
    return 0;
}