/* Задание 4.2 - Программа, которая с помощью массива указателей выводит слова
строки в обратном порядке */
#include <stdio.h>
#include <string.h>

#define MAXSTRING 256
#define MAXWORD 128

int main()
{
    char buf[MAXSTRING];
    char *wordBeg[MAXWORD] = { 0 }; // массив указателей для хранения адресов начала слов
    int n = 0, countPos = 0, countWords = 0;
    /* countPos - счетчик текущий позиции символа строки ;
    countWords- счетчик слов;
    n - флаг для подсчета слов и счетчик для вывода слов */

    puts("Enter a string and press <Enter>");
    fflush(stdin);
    fgets(buf, MAXSTRING, stdin);
    buf[strlen(buf) - 1] = 0;

    while (buf[countPos])
    {
        if (buf[countPos] != ' ' && n == 0) // работа со словами
        {
            n = 1;  // флаг - считываемый символ "в слове"
            countWords++;
            wordBeg[countWords] = &buf[countPos];
        }
        if (buf[countPos] == ' ' && n == 1)
            n = 0;
        countPos++;
    }
    while (countWords > 0)
    {
        for (n = 0; (*(wordBeg[countWords] + n)) != ' ' && (*(wordBeg[countWords] + n)) != 0; n++) // вывод слова посимвольно, начиная с текущего указателя и до пробела/конца строки
            putchar(*(wordBeg[countWords] + n));
        putchar(' ');
        countWords--;
    }
    putchar('\n');
    return 0;
}